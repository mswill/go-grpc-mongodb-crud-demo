FROM golang:1.20.2-alpine3.17 as builder

RUN echo '----------------- DEPLOY GO-GRPC-SERVER-MONGODB-CRUD-DEMO -----------------'
WORKDIR /app
RUN #apk add --update make  gcc
COPY ./go.mod ./
COPY ./go.sum ./
COPY . .

RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -o grpc-server-mongo-crud ./cmd/server/main.go

RUN echo '----------------- BUILD GO-GRPC-SERVER-MONGODB-CRUD-DEMO -----------------'
FROM alpine
COPY --from=builder /app/grpc-server-mongo-crud .
ENTRYPOINT [ "./grpc-server-mongo-crud" ]
