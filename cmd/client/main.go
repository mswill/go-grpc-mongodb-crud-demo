package main

import (
	"context"
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/mswill/go-grpc-mongodb-crud-demo/logg"
	pb "gitlab.com/mswill/go-grpc-mongodb-crud-demo/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"math/rand"
	"os"
	"time"
)

func init() {
	//initViper()
	rand.Seed(time.Now().Unix())
}

func main() {
	//grpcDial := viper.GetString("client.dial")
	//clientNetwork := viper.GetString("client.address")

	//
	l := logg.NewLogger()
	l.Setup()
	l.Log.Println("client grpc")
	//l.Log.Println("FROM VIPER ENV grpcDial", grpcDial)
	//l.Log.Println("FROM VIPER ENV clientNetwork", clientNetwork)

	server_port := os.Getenv("SERVER_DIAL_PORT")
	server_addr := os.Getenv("SERVER_ADDRESS")

	l.Log.Println("SERVER_DIAL_PORT ", server_port)
	l.Log.Println("SERVER_ADDRESS ", server_addr)

	strConn := fmt.Sprintf("%s:%s", server_addr, server_port)

	l.Log.Println("grpc client: ", strConn)
	conn, err := grpc.Dial(strConn, grpc.WithInsecure())
	if err != nil {
		l.Log.Fatalf("cannot listen grpc %v\n", err)
	}
	defer conn.Close()

	//
	cl := pb.NewBlogServiceClient(conn)
	createBlogUnary(cl, l)
	// readBlogPostUnary(cl, l)
	// updateBlogUnary(cl, l)
	// deleteBlogUnary(cl, l)
	// getALlRecords(cl, l)
}

func createBlogUnary(client pb.BlogServiceClient, l *logg.Logger) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	req := &pb.CreateBlogRequest{
		Blog: &pb.Blog{
			AuthorId: "1",
			Title:    "new title",
			Content:  "new content",
		},
	}
	res, err := client.CreateBlog(ctx, req)
	if err != nil {
		l.Log.Printf("cannot get response %v", err)
	}
	l.Log.Printf("resp : %v", res.GetBlog())
}
func readBlogPostUnary(client pb.BlogServiceClient, l *logg.Logger) {
	l.Log.Println("find blog unary ")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	//
	req := &pb.ReadBlogRequest{BlogId: "6415d82752905057e9ffba8b"}
	res, err := client.ReadBlog(ctx, req)
	if err != nil {
		s, ok := status.FromError(err)
		if ok {
			l.Log.Println(s.Message())
			return
		}
		l.Log.Printf("cannot get response %v\n", err)
		return
	}

	l.Log.Println("receive res: ")
	l.Log.Println("id ", res.GetBlog().GetId())
	l.Log.Println("author_id ", res.GetBlog().GetAuthorId())
	l.Log.Println("title ", res.GetBlog().GetTitle())
	l.Log.Println("content ", res.GetBlog().GetContent())
}
func updateBlogUnary(client pb.BlogServiceClient, l *logg.Logger) {
	l.Log.Println("client update blog unary")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	req := &pb.UpdateBlogRequest{Blog: &pb.Blog{
		Id:       "6415c3dd0234502a7f48fb39",
		AuthorId: fmt.Sprintf("%d", rand.Int31n(1000)),
		Content:  "new content",
		Title:    "new title",
	}}
	blog, err := client.UpdateBlog(ctx, req)
	if err != nil {
		l.Log.Println(err)
		return
	}

	l.Log.Println(blog.GetBlog())

}
func deleteBlogUnary(client pb.BlogServiceClient, l *logg.Logger) {
	l.Log.Println("client delete blog ")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	req := &pb.DeleteBlogRequest{Blog: &pb.Blog{Id: "6415c3dd0234502a7f48fb39"}}
	blog, err := client.DeleteBlog(ctx, req)
	if err != nil {
		l.Log.Println(err)

		return
	}
	l.Log.Println("delete response ", blog.GetRes())
}

// stream
func getALlRecords(client pb.BlogServiceClient, l *logg.Logger) {
	l.Log.Println("client get all records")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	req := &pb.GetAllRecordsRequest{}
	stream, err := client.GetAllRecords(ctx, req)
	if err != nil {
		l.Log.Println(err)
		return
	}
	for {
		res, err := stream.Recv()
		if err == io.EOF {
			l.Log.Println("not more data from server. end")
			break
		}
		if err != nil {
			l.Log.Println("cannot receive records ", err)
			return
		}
		l.Log.Println(res.GetBlog().GetId())
	}
}

// utils
func initViper() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("cannot read config.yaml file %v\n", err)
	}
	log.Printf(viper.GetString("server.port"))
}
