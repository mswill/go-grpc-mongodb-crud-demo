package main

import (
	"context"
	"github.com/spf13/viper"
	"gitlab.com/mswill/go-grpc-mongodb-crud-demo/logg"
	"gitlab.com/mswill/go-grpc-mongodb-crud-demo/mongodb"
	"go.mongodb.org/mongo-driver/bson"
	"os"
	"os/signal"
	"syscall"
	"time"

	pb "gitlab.com/mswill/go-grpc-mongodb-crud-demo/pb"
	"gitlab.com/mswill/go-grpc-mongodb-crud-demo/service"
	"google.golang.org/grpc"
	"net"
)

func init() {
	initViper()
}

func main() {
	serverAddress := viper.GetString("server.address")
	serverPort := viper.GetString("server.port")

	// logrus setup
	l := logg.NewLogger()
	l.Setup()
	l.Log.Println("server grpc")

	// mongodb setup
	mongoClient, err := mongodb.NewMongoClient("dbBlog", "blog", l, nil, nil)
	if err != nil {
		l.Log.Printf("cannot connect to mongodb %v\n ", err)
	}
	// test
	doc := bson.M{"name": "sergey", "l_name": "mataruev"}
	one, err := mongoClient.Collection.InsertOne(context.Background(), doc)
	if err != nil {
		l.Log.Println("mongo err: ", err)
		return
	}
	l.Log.Println("inserted document: ", one.InsertedID)
	// test end
	//
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM, syscall.SIGINT, os.Interrupt)

	//
	opts := []grpc.ServerOption{}
	grpcServer := grpc.NewServer(opts...)
	serverBlog := service.NewServerBlog(l, mongoClient)
	pb.RegisterBlogServiceServer(grpcServer, serverBlog)
	//reflection.Register(grpcServer)
	//
	listen, err := net.Listen("tcp", serverAddress+":"+serverPort)
	l.Log.Println("server is started: ", serverAddress+":"+serverPort)
	if err != nil {
		l.Log.Fatal(err)
	}
	go func() {
		l.Log.Println("starting server")
		err = grpcServer.Serve(listen)
		if err != nil {
			l.Log.Fatal(err)
		}
	}()

	// wait signal
	<-sigChan
	l.Log.Println("Graceful showdown throw 1 seconds")
	time.Sleep(time.Second)

	// to stopped all
	defer func() {
		_ = mongoClient.Client.Disconnect(context.Background())
		l.Log.Println("mongodb stopped")

		grpcServer.GracefulStop()
		l.Log.Println("server stopped")

		_ = listen.Close()
		l.Log.Println("listener stopped")

		l.Log.Println("Service stopped")
	}()
}
func initViper() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	err := viper.ReadInConfig()
	if err != nil {
		//log.Fatalf("cannot read config.yaml file %v\n", err)
	}

}
