package logg

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"runtime"
	"strings"
)

type Logger struct {
	Log *logrus.Logger
}

func NewLogger() *Logger {
	return &Logger{Log: logrus.New()}
}
func (l *Logger) Setup() {
	l.Log.SetReportCaller(true)
	formatter := &logrus.TextFormatter{
		TimestampFormat:        "02-01-2006 15:04:05", // the "time" field configuration
		FullTimestamp:          true,
		DisableLevelTruncation: true, // log level field configuration
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			return "", fmt.Sprintf(" %s:%d", formatFilePath(f.File), f.Line)
		},
	}
	l.Log.SetFormatter(formatter)
}

func formatFilePath(path string) string {
	arr := strings.Split(path, "/")
	return arr[len(arr)-1]
}
