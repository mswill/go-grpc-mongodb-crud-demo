package mongodb

import (
	"context"
	"gitlab.com/mswill/go-grpc-mongodb-crud-demo/logg"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
)

type MongoClient struct {
	Client     *mongo.Client
	Collection *mongo.Collection
}

func NewMongoClient(
	dbName string,
	collectionName string,
	l *logg.Logger,
	dbOpts []*options.DatabaseOptions,
	colOpts []*options.CollectionOptions,
) (*MongoClient, error) {
	// set config
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	// set new
	url := os.Getenv("MONGO_DB_SERVICE")
	l.Log.Printf("MONGO_DB_SERVICE: url: %s\n ", url)

	credential := options.Credential{
		Username: os.Getenv("MONGO_INITDB_ROOT_USERNAME"),
		Password: os.Getenv("MONGO_INITDB_ROOT_PASSWORD"),
	}
	l.Log.Println()
	l.Log.Println("MONGO_INITDB_ROOT_USERNAME ", credential.Username)
	l.Log.Println("MONGO_INITDB_ROOT_PASSWORD ", credential.Password)
	l.Log.Println()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://"+url).SetAuth(credential))
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		l.Log.Fatal(err)
	}

	// set db and collections options
	if dbOpts == nil {
		dbOpts = []*options.DatabaseOptions{}
	}
	if colOpts == nil {
		colOpts = []*options.CollectionOptions{}
	}

	collection := client.Database(dbName, dbOpts...).Collection(collectionName, colOpts...)

	l.Log.Printf("connect to mongo [ bbName:%s collectionName:%s ] ", dbName, collectionName)

	return &MongoClient{Collection: collection, Client: client}, nil
}
