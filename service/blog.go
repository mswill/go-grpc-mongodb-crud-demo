package service

import (
	"context"
	"gitlab.com/mswill/go-grpc-mongodb-crud-demo/logg"
	"gitlab.com/mswill/go-grpc-mongodb-crud-demo/mongodb"
	pb "gitlab.com/mswill/go-grpc-mongodb-crud-demo/pb"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type serverBLog struct {
	*logg.Logger
	db *mongodb.MongoClient
	pb.UnimplementedBlogServiceServer
}
type blogItem struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	AuthorID string             `bson:"author_id"`
	Content  string             `bson:"content"`
	Title    string             `bson:"title"`
}

func NewServerBlog(logger *logg.Logger, db *mongodb.MongoClient) *serverBLog {
	return &serverBLog{
		Logger: logger,
		db:     db,
	}
}

func (s *serverBLog) CreateBlog(ctx context.Context, req *pb.CreateBlogRequest) (*pb.CreateBlogResponse, error) {
	s.Log.Println("call CreateBlog")
	//

	err := s.contextErr(ctx)
	if err != nil {
		s.Log.Println(err)
		return nil, err
	}

	//
	//doc := blogItem{
	//	AuthorID: req.GetBlog().GetAuthorId(),
	//	Content:  req.GetBlog().GetContent(),
	//	Title:    req.GetBlog().GetTitle(),
	//}

	doc := blogItem{
		AuthorID: "11",
		Content:  "new content",
		Title:    "new title",
	}
	docRes, err := s.db.Collection.InsertOne(ctx, doc)
	if err != nil {
		return nil, s.errLog(status.Errorf(codes.Internal, "cannot insert the doc to mongodb", err))
	}

	docID, ok := docRes.InsertedID.(primitive.ObjectID)
	if !ok {
		return nil, s.errLog(status.Errorf(codes.Internal, "cannot cast to primitive objectID", nil))
	}
	//
	res := &pb.CreateBlogResponse{
		Blog: &pb.Blog{
			Id:       docID.Hex(),
			AuthorId: doc.AuthorID,
			Title:    doc.Title,
			Content:  doc.Content,
		},
	}

	return res, nil
}
func (s *serverBLog) ReadBlog(ctx context.Context, req *pb.ReadBlogRequest) (*pb.ReadBlogResponse, error) {
	s.Log.Println("ReadBlog receive req")
	//
	r := req.GetBlogId()
	docID, err := primitive.ObjectIDFromHex(r)
	if err != nil {
		return nil, s.errLog(err)
	}
	//
	err = s.contextErr(ctx)
	if err != nil {
		return nil, err
	}
	//
	data := &blogItem{}
	filter := bson.M{"_id": docID}
	if err := s.db.Collection.FindOne(ctx, filter).Decode(data); err != nil {
		return nil, s.errLog(status.Errorf(codes.Internal, "cannot decode to data", err))
	}
	//
	res := &pb.ReadBlogResponse{Blog: &pb.Blog{
		Id:       data.ID.Hex(),
		AuthorId: data.AuthorID,
		Title:    data.Title,
		Content:  data.Content,
	}}

	return res, nil
}
func (s *serverBLog) UpdateBlog(ctx context.Context, req *pb.UpdateBlogRequest) (*pb.UpdateBlogResponse, error) {
	s.Log.Println("update blog receive")

	err := s.contextErr(ctx)
	if err != nil {
		return nil, err
	}

	reqBlog := req.GetBlog()
	docID, err := primitive.ObjectIDFromHex(reqBlog.GetId())
	if err != nil {
		return nil, s.errLog(status.Errorf(codes.InvalidArgument, "invalid argument, cannot parse", err))
	}
	// find record if exists
	data := &blogItem{}
	filter := bson.M{"_id": docID}
	if err := s.db.Collection.FindOne(ctx, filter).Decode(data); err != nil {
		return nil, s.errLog(status.Errorf(codes.NotFound, "record not found", err))
	}

	// if exists make update
	opts := options.Update().SetUpsert(true)
	data.AuthorID = req.GetBlog().GetAuthorId()
	data.Title = req.GetBlog().GetTitle()
	data.Content = req.GetBlog().GetContent()
	updateFilter := bson.M{"$set": bson.M{
		"title":     data.Title,
		"content":   data.Content,
		"author_id": data.AuthorID,
	}}

	updatedDoc, err := s.db.Collection.UpdateOne(ctx, filter, updateFilter, opts)
	if err != nil {
		return nil, s.errLog(status.Errorf(codes.Internal, "cannot update document", err))
	}
	s.Log.Println("updated doc ", updatedDoc)

	//
	res := &pb.UpdateBlogResponse{Blog: &pb.Blog{
		Id:       data.ID.Hex(),
		AuthorId: data.AuthorID,
		Title:    data.Title,
		Content:  data.Content,
	}}
	return res, nil
}
func (s *serverBLog) DeleteBlog(ctx context.Context, req *pb.DeleteBlogRequest) (*pb.DeleteBlogResponse, error) {
	s.Log.Println("delete blog receive")

	err := s.contextErr(ctx)
	if err != nil {
		return nil, err
	}
	//
	hex := req.GetBlog().GetId()
	docID, err := primitive.ObjectIDFromHex(hex)
	if err != nil {
		return nil, s.errLog(status.Errorf(codes.InvalidArgument, "cannot parse", err))
	}
	//
	filter := bson.M{"_id": docID}
	deletedDoc, err := s.db.Collection.DeleteOne(ctx, filter)
	if err != nil {
		return nil, s.errLog(status.Errorf(codes.Internal, "cannot delete document", err))
	}
	s.Log.Println("deleted count : ", deletedDoc.DeletedCount)
	res := &pb.DeleteBlogResponse{Res: "document with " + hex + " was deleted"}
	return res, nil
}

// stream
func (s *serverBLog) GetAllRecords(req *pb.GetAllRecordsRequest, stream pb.BlogService_GetAllRecordsServer) error {
	s.Log.Println("get all records receive")

	//time.Sleep(time.Second * 6)
	err := s.contextErr(stream.Context())
	if err != nil {
		return err
	}
	var data []blogItem

	cursor, err := s.db.Collection.Find(stream.Context(), bson.M{})
	defer cursor.Close(context.Background())
	if err != nil {
		return s.errLog(status.Errorf(codes.NotFound, "records not found", err))
	}

	// get all records
	for cursor.Next(stream.Context()) {
		d := blogItem{}
		err := cursor.Decode(&d)
		if err != nil {
			return s.errLog(status.Errorf(codes.Internal, "cannot to decode", err))
		}
		data = append(data, d)
	}

	for _, record := range data {
		res := &pb.GetAllRecordsResponse{Blog: &pb.Blog{
			Id:       record.ID.Hex(),
			AuthorId: record.AuthorID,
			Title:    record.Title,
			Content:  record.Content,
		}}
		err := stream.Send(res)
		if err != nil {
			return s.errLog(status.Errorf(codes.Internal, "cannot send record", err))
		}
	}

	//if sent all records to client

	return nil
}

// utils
func (s *serverBLog) errLog(err error) error {
	if err != nil {
		s.Log.Println("-> ", err)
	}
	return err
}
func (s *serverBLog) contextErr(ctx context.Context) error {
	switch ctx.Err() {
	case context.Canceled:
		return s.errLog(status.Error(codes.Canceled, "client call is canceled"))
	case context.DeadlineExceeded:
		return s.errLog(status.Error(codes.DeadlineExceeded, "context deadline exceeded"))
	default:
		return nil
	}
}
