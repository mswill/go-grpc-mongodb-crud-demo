dep:
	go get go.mongodb.org/mongo-driver/mongo \
	google.golang.org/grpc \
	github.com/spf13/viper \
	github.com/go-playground/validator/v10 \
	github.com/sirupsen/logrus \
	github.com/emirpasic/gods \
	github.com/samber/lo@v1


buf_gen:
	./proto_gen.sh

server:
	go run ./cmd/server/main.go --race

client:
	go run ./cmd/client/main.go --race

# BUILD AND DEPLOY
dev:
	docker-compose -f dev-docker-compose.yaml up --build

prod:
	docker-compose -f docker-compose.yaml up --build

down:
	docker-compose down --remove-orphans

change:
	./change-env-minikube.sh

kup:
	./kube_apply.sh

kdown:
	./kube_destroy.sh


# show used CPU and MEMORY
define top =
clear
kubectl top -n grpc node
kubectl top -n grpc pod
endef

top: ; $(top)
.ONESHELL:

# show all in namespaces GRPC
define all =
clear
kubectl get all -n grpc -o wide
endef
all: ; $(all)
.ONESHELL:

