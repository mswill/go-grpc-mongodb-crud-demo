#!/bin/zsh


# mongo
kubectl apply -f mongo-pv.yaml
kubectl apply -f mongo-pvc.yaml
kubectl apply -f mongo-configmap.yaml
kubectl apply -f mongo-secret.yaml
kubectl apply -f mongo-service-deploy.yaml

# server
kubectl apply -f grpc-server-service-deploy.yaml

#kubectl apply -f grpc-client-service-deploy.yaml
