#!/bin/zsh

# mongo
#kubectl delete -f mongo-pv.yaml
#kubectl delete -f mongo-pvc.yaml
kubectl delete -f mongo-configmap.yaml
kubectl delete -f mongo-secret.yaml
kubectl delete -f mongo-service-deploy.yaml

# server
kubectl delete -f grpc-server-service-deploy.yaml

#kubectl delete -f grpc-client-service-deploy.yaml
